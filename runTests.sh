#!/bin/bash

set +e  # Be sure to continue executing upon failure to ensure teardown executes

readonly PROGNAME=$(basename ${0})
readonly PROGDIR=$(readlink -m $(dirname ${0}))
readonly TESTDIR=${PROGDIR}/test
readonly CONTAINER_BUILDSCRIPT=${TESTDIR}/docker-helper/build/make_sshnode.sh
readonly ARGS="${@}"

usage(){
    cat <<- EOF
Usage: ${PROGNAME} [REPLICATOR_TESTS...] [FLAGS...]

Ex. 
  ./${PROGNAME} -q -n -i    # Runs quick, noop, and install tests
  ./${PROGNAME} -a -k       # Runs all tests, skip teardown

Replicator Tests:
  -a, --all                 Run all tests
  -q, --quick               Run the replicator quick tests
  -n, --noop                Run the replicator no-op tests
  -i, --install             Run the replicator install tests
  -c, --copy-ssh-key        Run the replicator copy-ssh tests
  -b, --copy-install        Run the replicator copy-and-install tests
  -r, --regression          Run the replicator regression tests

Flags:
  -h, --help                Show this message
  -d, --notest              Skip tests
  -k, --keep-env            Skip teardown
  -t, --teardown            Teardown environment now (exits immediately after)
EOF
}

parse_args(){
    options=`getopt -o haqnicbrkdt --long help,all,quick,noop,install,copy-ssh-key,copy-install,regression,keep-env,notest,teardown -n "${PROGNAME}" -- "${@}"`
    eval set -- ${options}
    while true
    do
        case ${1} in
            -h|--help)
                usage ; exit 0 ;;
            -t|--teardown)
                teardown_env ; exit 0 ;;
            -k|--keep-env)
                TEARDOWN=false ; shift ;;
            -d|--notest)
                TEARDOWN=false ; TEST=false ; shift ;;
            -a|--all)  # Do not combine
                KOMM_TESTS="TEST_QUICK TEST_NOOP TEST_INSTALL TEST_COPY TEST_COPY_INSTALL TEST_REGRESSION" ;
                NUM_NODES=24 ; shift ; break ;;
            -q|--quick)
                add_test_flag "TEST_QUICK" ; add_nodecount 6 ; shift ;;
            -n|--noop)
                add_test_flag "TEST_NOOP" ; add_nodecount 4 ; shift ;;
            -i|--install)
                add_test_flag "TEST_INSTALL" ; add_nodecount 4 ; shift ;;
            -c|--copy-ssh-key)
                add_test_flag "TEST_COPY" ; add_nodecount 4 ; shift ;;
            -b|--copy-install)
                add_test_flag "TEST_COPY_INSTALL" ; add_nodecount 4 ; shift ;;
            -r|--regression)
                add_test_flag "TEST_REGRESSION" ; add_nodecount 1 ; shift ;;
            --)
                shift ; break ;;
        esac
    done
}

add_test_flag(){
    KOMM_TESTS="${KOMM_TESTS} ${1}"
}

add_nodecount(){
    NUM_NODES=$[${NUM_NODES}+${1}]
}

main(){
    local NUM_NODES=1  # Puppet node
    local TEARDOWN=true
    local TEST=true
    local KOMM_TESTS=""

    parse_args ${ARGS}

    export KOMM_TESTS  # So unittest can tell which tests its expected to run

    setup_env
    if ${TEST}; then
        run_tests
    fi
    if ${TEARDOWN}; then
        teardown_env
    else
        echo "TESTNODES ACTIVE AT THESE IPS:"
        for TESTNODE_IP in ${TESTNODE_IPS}; do
            echo "  - $(echo ${TESTNODE_IP} | cut -d ':' -f1)"
        done
    fi  
}

setup_env(){
	echo "    ======  runTest.sh: Setting Up Environment (${NUM_NODES} nodes)  ============="
    # Rebuild the sshnode container- ensures latest changes exist on puppet node
    ${CONTAINER_BUILDSCRIPT}

    # Create test network in docker so test nodes can communicate directly
    docker network create sshnode-network

    # Provision specified number of containers, export container IP and ID's for
    # the tests to pick up
    i=0
    while [ ${i} -lt ${NUM_NODES} ]; do
        CID=$(docker run -d -P --network sshnode-network sshnode)
        CIP=$(docker inspect ${CID} | grep 'IPAddress' | cut -d '"' -f4 | tr -d '\n' | tr -d ' ')
        TESTNODE_IPS="${TESTNODE_IPS}${CIP}:${CID} "
        i=$[${i}+1]
    done
    export TESTNODE_IPS
}

# Switch over to the test directory and invoke nose
run_tests(){
	echo "    ======  runTest.sh: Running Tests  ================================="
    pushd .
    cd ${TESTDIR}
    nosetests --nocapture -v
    popd
}


# Tear the env down, silently, so as to reduce scrolling back up to errors
teardown_env(){
	echo "    ======  runTest.sh: Tearing Down Test Nodes  ======================="

    # Kill running sshnode docker containers
	for ID in $(docker ps | grep 'sshnode' | cut -d ' ' -f 1); do
		docker kill ${ID} > /dev/null 2>&1
	done

    # Clean up extraneous containers, images, and the test network
	docker rm $(docker ps -a -q) > /dev/null 2>&1
	docker rmi $(docker images | grep "^<none>" | awk '{print $3}') > /dev/null 2>&1
	docker network rm sshnode-network > /dev/null 2>&1
}

main
