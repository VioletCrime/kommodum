# Kommodum
## What is it?
The name 'Kommodum' is derived from the latin 'commodus'/'commodum' for 'convenience'; it exists entirely to streamline my bash environment to increase my productivity in linux environments. Kommodum started out as a very long .bash_aliases file, containing several aliases and functions, and a highly-customized prompt. Working in the Cloud space at HP (later HPE), I found myself connecting to several new machines (virtual and physical) at least weekly to debug issues as they arose. As time went on and I grew more and more accustomed to my personalized environment, I wanted the ability to replicate it out to these other systems with a single command. As an added challenge, I needed the ability to only source my environment additions on a per-session basis if it was a system which I did not own.
From this, this project of three major components was born:
#### 1. Bash Environment Personalization (aliases/functions/prompt)
I ended up refactoring the large .bash_aliases into seperate files which focus on specific features, which are all sourced via a single entrypoint (`environment/src_env`). Additionally, several program confs are included, which would be moved to their appropriate locations at install script runtime.
#### 2. Install script
Automatically copies the Kommodum source directory to the user's home directory, under the hidden subdirectory `$HOME/.kommodum`, copies the program configuration files to their appropriate locations under the user's home directory, and appends the user's `.bashrc` to automatically source the environment whenever an interactive shell is invoked. If a configuration file already exists in the user's home directory, it will not be overwritten unless run with the overwrite command (which makes a backup up the original file).
#### 3. Replicator
Script which employs SSH and SCP to automatically copy the `$HOME/.kommodum` directory to the target system, and invokes an interactive SSH session for the user to employ as a standard vanilla ssh invokation. Can run the install script on the target system if directed, but if the .bashrc directive is not installed in the system default .bashrc, then bash is invoked manually with an alternate bashrc file which does have the environment-sourcing directive. This allows Kommodum users to enjoy their bash aliases, functions, and prompt on any machine, with minimal footprint. Additionally, the replicator will test the target system's Kommodum version, and if the source host's copy of Kommodum is newer, than the target system's Kommodum will be deleted and replaced with the newer bits. Finally, because the replicator is written 'on top' of SSH, it can be invoked with hostnames as they appear in your ssh config file, and will enjoy passwordless authentication if enabled.

## How do I install it?
Simply clone this repository, run `kommodum/src/install.sh`, and restart your bash session. To replicate Kommodum to other systems, use the `komm` alias (`kommodum/src/replicator/replicator.sh`). See `komm -h` for further details.


## What does it provide?
#### Customized PS1
![PS1 img](https://bitbucket.org/VioletCrime/kommodum/raw/0e3a15deb04330c429f10c456a220f61fefbb720/.README_images/PS1.png "PS1 in all its glory")
#### ACD_Func
Written by Petar Marinov (http://linuxgazette.net/109/misc/marinov/acd_func.html) with personalized aliases to reduce keystrokes, acd_func overrides `cd`, adding directory tracking, allowing a user to quickly switch among the last 10 directories they've visited.  
![acd_func img](https://bitbucket.org/VioletCrime/kommodum/raw/0e3a15deb04330c429f10c456a220f61fefbb720/.README_images/acd_func.png "acd_func in all its glory")
#### Recycle Bin
Written by me, overrides `rm`, moving target files and folders to `/tmp/recycle_bin`. Has saved me a few times.  
![recycle_bin img](https://bitbucket.org/VioletCrime/kommodum/raw/0e3a15deb04330c429f10c456a220f61fefbb720/.README_images/bash_recycle_bin.png "recycle bin in all its glory")
#### Aliases Galore!
Several aliases which reduce keystrokes to invoke commonly-used programs and functions.  
![aliases_galore img](https://bitbucket.org/VioletCrime/kommodum/raw/0e3a15deb04330c429f10c456a220f61fefbb720/.README_images/aliases.png "aliases in all their glory")
#### Program Configurations
Includes config files for:  
  Bash Aliases (\~/.bash_aliases)  
  Git (\~/.gitconfig)  
  SSH (\~/.ssh/config)  
  Terminator (\~/.config/terminator/config)  <-- The best terminal emulator, imo  
  Vim (\~/.vimrc)  
  
  

## Testing
Using either the Makefile targets, or invoking `./runTests.sh` directly (preferred), a docker container will be built (if it doesn't already exist), and up to 23 nodes will be provisioned (if running full test suite- ~300MB memory consumption). One of the nodes is designated as a 'source node', which all tests are run against. Tests which run the replicator target a new node from the pool, assuring a fresh environment for every test, ensuring test result accuracy.  
Developed with:
- Ubuntu 16.04 ( 4.4.0-83-generic #106-Ubuntu )
- docker.io 1.12.6-0ubuntu1~16.04.1

