#!/usr/bin/python

import paramiko
import random
import re
import time

#####  Replicator Command Generator  =======================================

replicator_script = '.kommodum/replicator/replicator.sh'
replicator_cmd_flags = "{0} {1}".format(replicator_script, "{0}-a {1} -u {2}")
replicator_cmd_noflags = "{0} {1}".format(replicator_script, "{0}{2}@{1}")
replicator_flags = {'install': '-i ',
                    'copy-ssh-id': '-c ',
                    'install-and-copy': '-i -c '}

def get_noop_cmd(target_node):
    return build_replicator_cmd(target_node)


def get_install_cmd(target_node):
    return build_replicator_cmd(target_node, replicator_flags['install'])


def get_copy_ssh_cmd(target_node):
    return build_replicator_cmd(target_node, replicator_flags['copy-ssh-id'])


def get_copy_and_install_cmd(target_node):
    return build_replicator_cmd(target_node, replicator_flags['install-and-copy'])


def build_replicator_cmd(target_node, flags=''):
    cmd = get_replicator_cmd()
    result = cmd.format(flags, target_node.ip, target_node.login)
    return result

def get_replicator_cmd():
    return replicator_cmd_flags if random.randint(1, 2) == 1 else replicator_cmd_noflags

#####  END Replicator Command Generator  ===================================


#####  Remote Execution Helper  ============================================

def run_cmd_on_host(cmd, node, target_node=None, debug=False):
    ssh = SSHWrapper(node)
    result = ssh.run_cmd(cmd, target_node)
    if debug:
        print_run_cmd_on_host_details(cmd, node.ip, result, target_node)  # Great debugging output
    ssh.close()
    return result


def print_run_cmd_on_host_details(cmd, ip, result, target_node):
    print "\n=================================="
    print "CMD: {0}".format(cmd)
    print "---"
    print "HOST: {0}".format(ip)
    print "---"
    print "RESULT: {0}".format(result)
    print "---"
    print "TGT_NODE: {0}".format(target_node)
    print "===================================="


class TimeoutException(Exception):
    pass


class SSHWrapper(object):
    def __init__(self, node=None):
        self.ssh = paramiko.client.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.ssh.save_host_keys('/dev/null')
        self.timeout = 5
        self.poll_interval = 0.05
        if node is not None:
            self.connect(node.get_connection_info())

    def connect(self, node):
        self.ssh.connect(**node)

    def close(self):
        self.ssh.close()

    def wait_for_ready(self, channel):
        while not channel.recv_ready():
            time.sleep(self.poll_interval)

    def sanitize_output(self, output):
        # Taken from https://stackoverflow.com/a/36948840
        return re.compile(r'(\x9B|\x1B\[)[0-?]*[ -/]*[@-~]').sub('', output).replace('\b', '').replace('\r', '')

    def run_cmd(self, cmd, target_node=None):
        if target_node is None:
            return self.run_basic_cmd(cmd)
        else:
            return self.run_replicator_cmd(cmd, target_node)

    def run_basic_cmd(self, cmd):
        transport = self.ssh.get_transport()
        channel = transport.open_session()
        channel.settimeout(20)
        channel.exec_command(cmd)
        rc = channel.recv_exit_status()  # Blocks
        stdout = channel.makefile('rb').read()
        stderr = channel.makefile_stderr('rb').read()
        return (rc, stdout, stderr)

    def run_replicator_cmd(self, cmd, target_node):
        complete_marker = "{0}@{1}$ ".format(target_node.login, target_node.hostname[:12])
        channel = self.ssh.invoke_shell()
        channel.settimeout(20)
        channel.send(cmd + '\n')
        self.wait_for_ready(channel)
        channel.send(target_node.password + '\n')
        self.wait_for_ready(channel)
        output = ''
        waited = 0
        while complete_marker not in output and waited < self.timeout:
            if not channel.recv_ready():
                time.sleep(self.poll_interval)
                waited += self.poll_interval
            else:
                output += self.sanitize_output(channel.recv(9999))
        if waited >= self.timeout:
            resp = "CMD '{0}' failed to display expected prompt within the timeout limit ({1} seconds)."
            raise TimeoutException(resp.format(cmd, self.timeout))
        return output
