#!/usr/bin/python

import unittest
from common import *
from util import run_cmd_on_host, get_noop_cmd, get_install_cmd,\
                 get_copy_ssh_cmd, get_copy_and_install_cmd

nopass_chk_cmd = "ssh -o BatchMode=yes -o StrictHostkeyChecking=no {0}@{1} 'exit 0'"
get_version_cmd = "cat ~/.kommodum/version"
set_version_cmd = "echo {0} > ~/.kommodum/version"

nopass_fail_rc = 255
skip_msg = "Test not specified"

tests = {'noop': {'cmd_generator': get_noop_cmd,
                  'nopass_enabled': False,
                  'installed': False},
         'copy': {'cmd_generator': get_copy_ssh_cmd,
                  'nopass_enabled': True,
                  'installed': False},
         'install': {'cmd_generator': get_install_cmd,
                     'nopass_enabled': False,
                     'installed': True},
         'copy-and-install': {'cmd_generator': get_copy_and_install_cmd,
                              'nopass_enabled': True,
                              'installed': True}
        }


class TestReplicator(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.nodes = get_vm_info()
        cls.src_node = cls.nodes.pop()  # Node which will issue all replicator commands
        run_cmd_on_host(install_script, cls.src_node)

    #  ==========  Helpers  ============================================================

    def bump_version_and_run_replicator(self, install=False, debug=False):
        target_node = self.nodes.pop()

        func = get_install_cmd if install else get_noop_cmd
        install_rc = 0 if install else install_fail_rc

        # Set Up - run the replicator against the target to lay down version file on target
        run_cmd_on_host(func(target_node), self.src_node, target_node, debug=debug)
        # Setup - source node version ++
        version = self.bump_src_node_version(debug=debug)
        # Test - rerun replicator against the target node
        run_cmd_on_host(get_noop_cmd(target_node), self.src_node, target_node, debug=debug)

        # Verify - Target node should indicate new version, but not be installed
        self.assertInstalledRC(target_node, install_rc)
        self.assertNodeSrcVersion(target_node, version)

    def bump_src_node_version(self, debug=False):
        version = self.get_node_version(self.src_node, debug=debug) + 1
        bump_result = run_cmd_on_host(set_version_cmd.format(version), self.src_node, debug=debug)
        self.assertEquals(bump_result[0], 0)
        self.assertNodeSrcVersion(self.src_node, version)
        return version

    def assertNodeSrcVersion(self, node, version):
        self.assertEquals(self.get_node_version(node), version)

    def get_node_version(self, node, debug=False):
        return int(run_cmd_on_host(get_version_cmd, node, debug=debug)[1].replace('\n', ''))

    def assertInstalledRC(self, host, expected_rc=install_fail_rc):
        check_result = run_cmd_on_host(install_chk_cmd, host)
        self.assertEquals(check_result[0], expected_rc, check_result)

    def assertNopassRC(self, src_host, dest_host, expected_rc=nopass_fail_rc):
        if expected_rc is None:
            expected_rc = self.nopass_fail_rc
        cmd = nopass_chk_cmd.format(dest_host.login, dest_host.ip)
        check_result = run_cmd_on_host(cmd, src_host)
        self.assertEquals(check_result[0], expected_rc, check_result)

    def assertPreConditions(self, target_node):
        self.assertNopassRC(self.src_node, target_node) #  Assert src_node doesn't have nopass to target node
        self.assertInstalledRC(self.src_node, 0)  # Assert src_node has the env installed
        self.assertInstalledRC(target_node)  # Assert target node doesn't have the env installed

    def assertPostConditions(self, target_node, nopass_enabled=False, installed=False):
        # Set expected return codes for the two verification tests
        nopass_rc = 0 if nopass_enabled else nopass_fail_rc
        install_rc = 0 if installed else install_fail_rc

        # Run verification of target node state using expected return codes
        self.assertNopassRC(self.src_node, target_node, nopass_rc)
        self.assertInstalledRC(target_node, install_rc)

    def run_cmds_and_check(self, tests, debug=False):
        target_node = self.nodes.pop()
        self.assertPreConditions(target_node)
        install_expected = False
        nopass_expected = False
        for test in tests:
            install_expected = install_expected or test['installed']
            nopass_expected = nopass_expected or test['nopass_enabled']
            run_cmd_on_host(test['cmd_generator'](target_node), self.src_node, target_node, debug=debug)
            self.assertPostConditions(target_node,
                                      nopass_enabled=nopass_expected,
                                      installed=install_expected)

    def get_paths(self, first_test_name):
        return [[tests[first_test_name], test] for test in tests.values()]

    #  ==========  END Helpers  ========================================================


    #  ==========  Tests  ==============================================================

    # Test each replicator ability by itself
    @unittest.skipUnless('TEST_QUICK' in testlist, skip_msg)
    def test_replicator_single_operations(self):
        for test in [test for test in tests.values()]:
            self.run_cmds_and_check([test])

    # Test replicator upgrade functionality
    @unittest.skipUnless('TEST_QUICK' in testlist, skip_msg)
    def test_replicator_upgrade_function_not_installed(self):
        self.bump_version_and_run_replicator(install=False, debug=False)

    # Test replicator upgrade functionality
    @unittest.skipUnless('TEST_QUICK' in testlist, skip_msg)
    def test_replicator_upgrade_function_installed(self):
        self.bump_version_and_run_replicator(install=True, debug=False)

    # Test each replicator ability after a noop command
    @unittest.skipUnless('TEST_NOOP' in testlist, skip_msg)
    def test_replicator_noop_execution_tree(self):
        for path in self.get_paths('noop'):
            self.run_cmds_and_check(path)

    # Test each replicator ability after an install command
    @unittest.skipUnless('TEST_INSTALL' in testlist, skip_msg)
    def test_replicator_install_execution_tree(self):
        for path in self.get_paths('install'):
            self.run_cmds_and_check(path)

    # Test each replicator ability after a copy command
    @unittest.skipUnless('TEST_COPY' in testlist, skip_msg)
    def test_replicator_copy_execution_tree(self):
        for path in self.get_paths('copy'):
            self.run_cmds_and_check(path)

    # Test each replicator ability after a copy-and-install command
    @unittest.skipUnless('TEST_COPY_INSTALL' in testlist, skip_msg)
    def test_replicator_copy_and_install_execution_tree(self):
        for path in self.get_paths('copy-and-install'):
            self.run_cmds_and_check(path)

    # Test KOMM-40: Replicator install resets HOSTCOLOR variable
    @unittest.skipUnless('TEST_REGRESSION' in testlist, skip_msg)
    def test_replicator_install_preserves_hostcolor(self):
        target_node = self.nodes.pop()
        self.assertPreConditions(target_node)
        run_cmd_on_host(get_install_cmd(target_node), self.src_node, target_node)
        run_cmd_on_host("sed -i 's/export HOSTCOLOR=/export HOSTCOLOR=FGYLBD/g' ~/.bashrc", target_node)
        setup_result = run_cmd_on_host('cat ~/.bashrc | grep "export HOSTCOLOR=FGYLBD"', target_node)
        self.assertEquals(setup_result[0], 0)
        run_cmd_on_host(get_install_cmd(target_node), self.src_node, target_node)
        test_result = run_cmd_on_host('cat ~/.bashrc | grep "export HOSTCOLOR=FGYLBD"', target_node)
        self.assertEquals(test_result[0], 0)

