#!/usr/bin/python

import unittest
from common import *
from util import run_cmd_on_host

class TestInstaller(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.nodes = get_vm_info()
        cls.src_node = cls.nodes.pop()
        run_cmd_on_host(install_script, cls.src_node)

    #  ==========  Helpers  ============================================================

    # Takes a command (string), and invokes the util function to run on the src_node, then
    # asserts the return value. If degub is set to true, the util function will print the cmd
    # and its result.
    def run_cmd(self, cmd, expected_rc=0, debug=False):
        err_msg = "\n    Command '{0}'\n    Result: '{1}'\n".format(cmd, '{0}')
        cmd_result = run_cmd_on_host(cmd, self.src_node, debug=debug)
        self.assertEquals(cmd_result[0], expected_rc, err_msg.format(cmd_result, cmd_result[0]))

    # Takes a function which will generate commands, a list of values to call the
    # function with, and calls run_cmd for each command generated, passing along
    # expected rc and the debug flag
    def run_cmd_for(self, cmd_func, lst, expected_rc=0, debug=False):
        for elem in lst:
            self.run_cmd(cmd_func(elem), expected_rc, debug)

    # Non-interactive terminals don't auto-source aliases and the like. These prepend commands
    # direct bash to go ahead and source 'em anyways. Can't source .bashrc files, as the very first
    # thing most of them do amounts to 'if non-interactive, exit 0', which this doesn't overcome.
    def prepend_source(self, cmd):
        return "shopt -s expand_aliases; \
                source $HOME/.kommodum/environment/src_env; \
                {0}".format(cmd)

    def assertInstalled(self, expected_rc=0, debug=False):
        self.run_cmd(install_chk_cmd, expected_rc, debug)

    # Given a path (string), assert it exists or not (default) on src_node
    def get_path_verify_cmd(self, path):
        return "if [ -e {0} ]; then exit 0; else exit 1; fi".format(path)

    def get_alias_verify_cmd(self, alias):
        return self.prepend_source("alias {0}".format(alias))

    def get_function_verify_cmd(self, func):
        return self.prepend_source("typeset -F | grep '{0}'".format(func))

    #  ==========  END Helpers  ========================================================


    #  ==========  Tests  ==============================================================

    def setUp(self):
        self.assertInstalled()

    # All of the other tests in this suite assert 0 return value- it's worthwhile to validate
    # that they return non-zero if a command fails
    def test_cmds_can_fail(self):
        self.run_cmd_for(self.get_alias_verify_cmd, ['notAnAlias'], 1)
        self.run_cmd_for(self.get_function_verify_cmd, ['notAFunction'], 1)
        self.run_cmd_for(self.get_path_verify_cmd, ['/not/a/pat'], 1)

    # Assert that at every root directory element exists, and at least one max-depth file
    # exists from every subdirectory
    def test_installer_source_dir_copied(self):
        paths = ['$HOME/.kommodum/environment/bash_commands/bash_prompt',
                 '$HOME/.kommodum/environment/program_confs/ssh_config',
                 '$HOME/.kommodum/bin/watch-and-run',
                 '$HOME/.kommodum/install.sh',
                 '$HOME/.kommodum/replicator/upload_dir_to_host.exp',
                 '$HOME/.kommodum/version']
        self.run_cmd_for(self.get_path_verify_cmd, paths)

    # Assert that at least one alias from every alias-specifying file is in the env
    def test_installer_aliases_available(self):
        aliases = ['c4',  # acd_func
                   'sz',  # bash_aliases
                   'gitcleanf',  # bash_git_cmds
                   'gr',  # bash_go_env
                   'proxy',  # bash_proxy
                   'permrm']  # bash_recycle_bin
        self.run_cmd_for(self.get_alias_verify_cmd, aliases)

    # Not all of the env's shortcuts are aliases- there are a few commands available as functions
    def test_installer_functions_available(self):
        functions = ['cd_func',  # acd_func
                     'clean-docker-objects',  # bash_clean_docker
                     #'gitClone',  # bash_git_cmds
                     'changehostcolor',  # bash_hostcolor_cmd
                     'my_set_bash_prompt',  # bash_prompt
                     'setProxy',  # bash_proxy_cmds
                     'sudoRmForReal']  # bash_recycle_bin
        self.run_cmd_for(self.get_function_verify_cmd, functions)

    # Assert each program conf is installed correctly to the user's environment
    def test_installer_program_confs_copied(self):
        paths = ['$HOME/.bash_aliases',
                 '$HOME/.config/terminator/config',
                 '$HOME/.gitconfig',
                 '$HOME/.ssh/config',
                 '$HOME/.vimrc']
        self.run_cmd_for(self.get_path_verify_cmd, paths)

    def test_installer_backup_and_replace_functionality(self):
        testfile = '$HOME/.vimrc'
        bakfile = '{0}-2*'.format(testfile)  # Test will begin failing at the dawn of the year 3000

        # Verify the base file exists, but no backup of it does
        self.run_cmd_for(self.get_path_verify_cmd, [testfile])
        self.run_cmd_for(self.get_path_verify_cmd, [bakfile], 1)

        # Append some junk data onto the test file, modifying its md5
        append_cmd = "echo 'junk data' >> {0}".format(testfile)
        result = run_cmd_on_host(append_cmd, self.src_node)

        # Rerun the installer, but without overwrite flag, so no backup file should exist
        run_cmd_on_host("{0} -s".format(install_script), self.src_node)

        # Verify both the base file and the new backup exist
        self.run_cmd_for(self.get_path_verify_cmd, [testfile])
        self.run_cmd_for(self.get_path_verify_cmd, [bakfile], 1)

        # Append more junk, and rerun the installer with overwrite flag, which should
        # generate the backup file we are looking for
        result = run_cmd_on_host(append_cmd, self.src_node)
        run_cmd_on_host(install_script, self.src_node)

        # Verify both the base file and the new backup exist
        self.run_cmd_for(self.get_path_verify_cmd, [testfile])
        self.run_cmd_for(self.get_path_verify_cmd, [bakfile])
