#!/usr/bin/python

import os

install_script = 'kommodum/install.sh'
install_marker = "export HOSTCOLOR="
install_chk_cmd = "cat ~/.bashrc | grep '{0}' > /dev/null".format(install_marker)
install_fail_rc = 1

testlist = os.environ['KOMM_TESTS']

def get_vm_info():
    login = 'root'
    password = 'docker'

    result = []
    ips = os.environ['TESTNODE_IPS']
    for ip in ips.split(' '):
        if ip != '':
            result.append(TestNode(login, password, ip.split(':')[0], ip.split(':')[1]))
    return result


class TestNode(object):
    def __init__(self, login, password, ip, hostname):
        self.login = login
        self.password = password
        self.ip = ip
        self.hostname = hostname  # Docker SHA

    # Useful when debugging
    def __repr__(self):
        return repr({'username': self.login,
                     'password': self.password,
                     'ip': self.ip,
                     'hostname': self.hostname})

    # Get a dict that can be passed as kwargs to paramiko.connect()
    def get_connection_info(self):
        return {'username': self.login,
                'password': self.password,
                'hostname': self.ip}
