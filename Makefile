# This is pretty much only here for demonstrative purposes-
# generally best to invoke runTests.sh directly

REPLICA_TGT_DOCKERTAG = kommodum-replica-tgt:latest
SHELL_DOCKERTAG = kommodum-shell:latest
MAKEFILE_PATH = $(shell pwd)

.PHONY: install
install:
	./src/install.sh

.PHONY: version
version:
	./scripts/version

.PHONY: ishell
ishell: shell-docker
	docker run -it --rm -v ${MAKEFILE_PATH}:/kommodum ${SHELL_DOCKERTAG} /bin/bash

.PHONY: shell
shell: shell-docker
	docker run -it --rm -v ${MAKEFILE_PATH}:/kommodum ${SHELL_DOCKERTAG}

.PHONY: shell-docker
shell-docker:
	docker build -t ${SHELL_DOCKERTAG} -f docker/shell.Dockerfile .

.PHONY: replica-tgt-docker
replica-tgt-docker:
	docker build -t ${REPLICA_TGT_DOCKERTAG} -f docker/replica_tgt.Dockerfile .

teardown :
	./runTests.sh -t

.PHONY: test
test :
	./runTests.sh

test-all :
	./runTests.sh -a

test-copy :
	./runTests.sh -c

test-copy-install :
	./runTests.sh -b

test-install-copy : test-copy-install

test-install :
	./runTests.sh -i

test-keep :
	./runTests.sh -a -k

test-noop :
	./runTests.sh -n

test-quick :
	./runTests.sh -q
