#!/usr/bin/env bash

#######################################################################################################
# This script assists tracking software versions via Git tags. The end user is expected to manage
# major and minor version increments, but this script automates incrementing the version when appropriate.
# This script offers a `--tag` option, which would automatically update the local git repo with the version
# if it was updated. Automation of the `git push --tags` step is left to the upstream automation facility.
#
# ASSUMPTIONS:
# * This script assumes that it is operating in a git repository where the only tags are
# version tags. This script can be augmented to search for tags given a particular pattern, but it's
# not there now. Buyer beware.
#
# * This script also assumes that your version strings are not simply an iterating integar and nothing
# else- the regex looks for a non-digit character preceeding an integer at the end of the version
# string. Ex:
#    Good version strings:  '1.0.1', '1.2-rc17', 'v1'
#    Bad version strings: '1', '9999', '5.5.5-prerelease'
#
# EXPECTED BEHAVIOR:
# If no changes have been detected since the last tag was written, simply output the value
# of the most recent tag.
#
# If uncommitted changes are detected, a '-dirty' suffix is appended to the version before being
# printed to stdout. This is avoidable using the -n/--no-dirty flag. Ex.
# 7.2.4 -> 7.2.4-dirty
#
# Finally, if new commits have been detected, the version is either altered or appended, depending
# on whether the current branch is configured in this script as a 'mainline branch', which is to
# say, 'a branch on which new commits constitute an increment of a version counter'. If the current
# branch IS a 'mainline branch', the number at the very end of the version string is incremented. Ex.
# 1.0.4 -> 1.0.5
# 2.1.0-prerelease17 -> 2.1.0-prerelease18
#
# Mainline branches are defined here as space-separated strings-
readonly MAINLINE_BRANCHES="master main"
#
# On all other branches, if changes have been merged since the last tag was written, instead of
# incrementing the version, the first 20 characters of the branch name are appended to the version.
# Ex.
# 3.2.1-rc7 -> 3.2.1-rc7-DEVOPS-2269_implemen
# 2.9.6 -> 2.9.6-DEVOPS-666_mark_of_t-dirty
#
# HOW FLAGS ALTER BEHAVIOR:
# '--no-dirty' simply skips the step of appending the '-dirty' suffix to the version if it would
# otherwise be written, an all execution paths.
#
# '--tag' directs the script to write a new git tag with the new version, but ONLY if the version
# was incremented on a 'mainline branch'. Additionally, the '-dirty' suffix is ignored at tag-time,
# though the '-dirty' flag will still be emitted to stdout if appropriate.
#
# While this script will support most versioning patterns out of the box, I highly recommend
# conforming to the Semantic Version specifications- https://semver.org/spec/v2.0.0.html
######################################################################################################

set -e

readonly PROGNAME=$(basename ${0})
readonly PROGDIR=$(readlink -m $(dirname ${0}))
readonly ARGS="${@}"

usage() {
    cat <<- EOF
Usage: ${PROGNAME} OPTIONS...
Options:
    -h, --help        Show this message and exit
    -n, --no-dirty    Forego the '-dirty' suffix if uncommitted changes have been detected.
    -t, --tag         Tags the git repo if the version has been incremented; does not push.
EOF
}

parse_args() {
    options=`getopt -o hnt --long help,no-dirty,tag -n "${PROGNAME}" -- "${@}"`
    eval set -- ${options}
    while true
    do
        case $1 in
            -h|--help)
                usage ; exit 0 ;;
            -n|--no-dirty)
                NO_DIRTY=true ; shift ;;
            -t|--tag)
                TAG=true ; shift ;;
            --)
                shift ; break ;;
        esac
    done
}

# Set user arg defaults before parsing arguments, possibly changing said defaults
NO_DIRTY=false
TAG=false
parse_args ${ARGS}

# CWD should be the root of the git repository this script resides in.
cd ${PROGDIR}
cd $(git rev-parse --show-toplevel)

# Interrogate git to discover the state of the repository
readonly LATEST_MERGED_TAG=$(git describe --abbrev=0 --tags)
readonly CURRENT_GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)

# Build the state-aware version string, starting with the base.
VERSION=${LATEST_MERGED_TAG}

# Case- new commits exist since latest tag
if [[ $(git rev-list ${LATEST_MERGED_TAG}..HEAD --count) -gt 0 ]]; then
    # Case- new commits exist, and we are on a version-incrementing branch
    if [[ " ${MAINLINE_BRANCHES[*]} " =~ " ${CURRENT_GIT_BRANCH} " ]]; then
        # Parse out the number at the end of the string, increment it, and reassemble the components
        regex="(.*[^0-9])([0-9]+)$"
        if [[ ${LATEST_MERGED_TAG} =~ ${regex} ]]; then
            counter=$((${BASH_REMATCH[2]}+1))
            VERSION="${BASH_REMATCH[1]}${counter}"
            if [[ "${TAG}" == true ]]; then  # Don't want '--dirty' in tag names; not a problem that logic is later
                git tag ${VERSION}
            fi
        else  # May be better to just tack a '0' onto the latest tag, instead of erroring?
            >&2 echo "ERROR: Latest tag \"${LATEST_MERGED_TAG}\" does not end with an expected integer to increment!"
            exit 1
        fi
    else  # Case- new commits exist, and we are on a branch with doesn't auto-increment the semver- add branch string
        VERSION="${VERSION}-${CURRENT_GIT_BRANCH:0:19}"
    fi  
fi
# Case- repo state is 'dirty'
if [[ `git status --porcelain` != "" ]] && [[ "${NO_DIRTY}" == false ]]; then
    VERSION="${VERSION}-dirty"
fi

echo "${VERSION}"