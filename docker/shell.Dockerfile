FROM ubuntu:20.04

RUN apt-get update \
    && apt-get install -yq tzdata \
    && ln -fs /usr/share/zoneinfo/America/Denver /etc/localtime \
    && dpkg-reconfigure -f noninteractive tzdata \
    && apt-get install -y expect git make vim \
    && rm -rf /var/lib/apt/lists/*

ARG COPY_DIR=/kommodum
RUN mkdir ${COPY_DIR}
COPY . ${COPY_DIR}
WORKDIR ${COPY_DIR}

RUN chown -R root:root ${COPY_DIR} \
    && git config --global --add safe.directory ${COPY_DIR}

CMD make install; bash
