FROM ubuntu:20.04

USER root

RUN apt-get update && apt-get install -y openssh-server expect vim && rm -rf /var/lib/apt/lists/*
RUN mkdir /var/run/sshd
RUN echo "root:docker" | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

EXPOSE 22

RUN echo "UseDNS no" >> /etc/ssh/sshd_config

RUN ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa

RUN mkdir /root/kommodum/
ADD src /root/kommodum

CMD ["/usr/sbin/sshd", "-D"]
