#!/usr/bin/env bash

set -e

readonly PROGNAME=$(basename ${0})
readonly PROGDIR=$(readlink -m $(dirname ${0}))
readonly ARGS="${@}"

readonly INSTALL_DIR="${HOME}/.kommodum"
readonly CONF_DIR=${INSTALL_DIR}/environment/program_confs
readonly ACTIVE_CONF_DIR=${INSTALL_DIR}/active_confs
readonly ENV_ENTRYPOINT=${INSTALL_DIR}/environment/src_env
readonly BASHRC="${HOME}/.bashrc"
readonly EXEC_DATE=`date +%F-%R`  # Date to append to backup filenames

usage() {
    cat <<- EOF
Usage: ${PROGNAME} [OPTIONS...]

Options:
  -h, --help                Show this message
  -s, --skip-backup         Don't back up overwritten conf files
EOF
}

parse_args(){
    options=`getopt -o hos --long help,overwrite,skip-backup -n "${PROGNAME}" -- "${@}"`
    eval set -- ${options}
    while true
    do
        case ${1} in
            -h|--help)
                usage ; exit 0 ;;
            -s|--skip-backup)
                SKIP_BACKUP=true ; shift ;;
            -o|--overwrite)
                OVERWRITE=true ; shift ;;
            --)
                shift ; break ;;
        esac
    done
}

main(){
    local SKIP_BACKUP=
    local OVERWRITE=
    parse_args ${ARGS}
    copy_sources_to_user_dir
    ensure_bashrc_entry
    copy_confs
    echo "Installation successful. Please (re)start the bash session for changes to take effect."
    exit 0
}

append_bashrc(){
    echo "${1}" >> ${BASHRC}
}

# Copy the source directory to the user's home directory
copy_sources_to_user_dir() {
    # Safeguard installing from installed location
    if [ ! "${INSTALL_DIR}" == "${PROGDIR}" ]; then
        # If the target directory already exists, remove it with prejudice.
        if [ -d ${INSTALL_DIR} ]; then
            command rm -rf ${INSTALL_DIR}
        fi
        # (Re)make the target directory, and copy the source directorys contents to it.
        mkdir -p ${INSTALL_DIR}
        cp -R -f ${PROGDIR}/* ${INSTALL_DIR}
        if [[ ! -e ${INSTALL_DIR}/version ]]; then
            if [[ -e ${PROGDIR}/../scripts/version ]]; then
                ${PROGDIR}/../scripts/version > ${INSTALL_DIR}/version
            else
                echo "Unknown; v41+, at least" > ${INSTALL_DIR}/version
            fi
        fi
    fi
}

ensure_bashrc_entry(){
    # If the hostcolor has been set, preserve it
    if [[ ! -z $(cat ${BASHRC} | grep "export HOSTCOLOR=") ]]; then
        HOSTCOLOR=$(cat ${BASHRC} | grep "export HOSTCOLOR=" | cut -d '=' -f2)
    else
        HOSTCOLOR=
    fi

    # Define the .bashrc entry
    SET_HOSTCOLOR="export HOSTCOLOR="
    IF_ENTRYPOINT="if [[ -f ${ENV_ENTRYPOINT} ]]; then"
    SRC_ENTRYPOINT="    . ${ENV_ENTRYPOINT}"
    FI_ENTRYPOINT="fi"

    # Delete and rewrite the .bashrc entry
    sed -i "/${SET_HOSTCOLOR}/,/${FI_ENTRYPOINT}/d" ${BASHRC}

    # Write out the .bashrc entry if it doesn't exist
    if ! cat ${BASHRC} | grep "${SET_HOSTCOLOR}" > /dev/null ; then
        append_bashrc "${SET_HOSTCOLOR}${HOSTCOLOR}"
        append_bashrc "QUOTING_STYLE=literal"
        append_bashrc "${IF_ENTRYPOINT}"
        append_bashrc "${SRC_ENTRYPOINT}"
        append_bashrc "    export PATH=\${PATH}:${INSTALL_DIR}/bin"
        append_bashrc "${FI_ENTRYPOINT}"
    fi
}

backup_and_replace(){
    local source_file=${1}
    local target_file=${2}
    if [ ! -e ${source_file} ]; then
        "Warning: Could not find file '${source_file}' to copy!"
        return
    fi
    if [[ -e ${target_file} && ! -z ${OVERWRITE} ]]; then
        if [[ ! `md5sum ${source_file} | cut -d ' ' -f1` == `md5sum ${target_file} | cut -d ' ' -f1` ]] ; then
            if [[ ! ${SKIP_BACKUP} ]]; then
                backup_file=$target_file-${EXEC_DATE}\.bak
                mv ${target_file} ${backup_file}
                echo "Replacing ${target_file} - The original has been moved to ${backup_file}"
            fi
            cp -f -R ${source_file} ${target_file}
        fi
    elif [[ ! -e ${target_file} ]]; then
        cp -f -R ${source_file} ${target_file}
    fi
}

copy_confs(){
    mkdir -p ${HOME}/.ssh
    mkdir -p ${HOME}/.config/terminator
    backup_and_replace ${CONF_DIR}/ssh_config ${HOME}/.ssh/config
    backup_and_replace ${CONF_DIR}/vimrc ${HOME}/.vimrc
    backup_and_replace ${CONF_DIR}/gitconfig ${HOME}/.gitconfig
    backup_and_replace ${CONF_DIR}/terminator_config ${HOME}/.config/terminator/config
    backup_and_replace ${CONF_DIR}/local_bash_aliases ${HOME}/.bash_aliases
}

main
