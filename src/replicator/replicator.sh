#!/bin/bash

set +e

readonly PROGNAME=$(basename ${0})
readonly PROGDIR=$(readlink -m $(dirname ${0}))
readonly ARGS="${@}"

readonly CONFDIR=$(readlink -m $(dirname ${PROGDIR}))
readonly SSH_EXP_CMD=${PROGDIR}/run_remote_command.exp
readonly SSH_EXP_CMD_INTERACTIVE=${PROGDIR}/run_remote_command_interactive.exp
readonly SCP_EXP_CMD=${PROGDIR}/upload_dir_to_host.exp
readonly SSH_CP_ID=${PROGDIR}/ssh_copy_id.exp
readonly VERSION=$(cat ${CONFDIR}/version)
readonly CONFDIR_DEST=".kommodum"  # Need to keep this in-line with installer, line 9

# Creates case-statement using exit codes.
# 33 - Source directory needs to be copied to target system, either becuase it's not
#        there, or because it's out of date
# 34 - Source directory needs to be copied, because it's out of date, and the old version
#        was installed, so we'll want to be sure to run the new version's installer once
#        copied to the target system
# If neither of the top-level if/elif statements evaluate true, then additional commands
# can be executed in-line, which we do later, with the START_SSH_CHECK command below
readonly SYSTEM_CHECK_CMD="if ! ls -la | grep ${CONFDIR_DEST} > /dev/null; then exit 33; else if [[ \$(cat ~/${CONFDIR_DEST}/version) -lt ${VERSION} ]]; then if grep 'export HOSTCOLOR=' ~/.bashrc > /dev/null; then exit 34; else exit 33; fi fi fi;"

# Run just before the user receives a prompt on the target system, this statement checks
# to see if the target system has the bashrc entry which sources the environment.
# If the bashrc does have the entry, normal operation.
# Otherwise, specify a custom bashrc instead of default, which will source the env, but
# only for this session.
readonly START_SSH_CHECK="if grep 'export HOSTCOLOR=' ~/.bashrc >/dev/null; then bash -l; else bash --init-file ~/${CONFDIR_DEST}/replicator/alt_bashrc; fi"


usage(){
    cat <<- EOF
    usage: ${PROGNAME} < -h | -a HOST >  [ -u USER, -p PORT, -i, -c ]

    This program will copy the custom bash directory to the specified host system if necessary, and then
    create a new interactive ssh session for the user to use, employing the copied confs. 
    This program does NOT modify the destination system to automatically source this configuration, allowing
    use of the confs, while playing nice with others using the same system account.

    OPTIONS:
        -h --help            Show this help
        -a --address         Hostname or IP of destination system. May also be ssh config entry, in which case 
                               'user' and 'port' options are unused
        -u --user            User to connect to destination system as.
        -p --port            Port to connect to destination system with (if other than 22)
        -i --install         Install the bash configuration
        -c --copy-ssh        Automatically run ssh-copy-id for passwordless ssh
        -d --debug           Enable debug mode
EOF
}

parse_args() {
    # Define and parse args
    options=`getopt -o hicdp:a:u: --long help,install,copyssh,dubug,port:,address:,user: -n "${PROGNAME}" -- "${@}"`
    eval set -- ${options}
    while true
    do
        case ${1} in
            -h|--help)
                usage ; exit 0 ;;
            -u|--user)
                USER=${2} ; shift 2 ;;
            -a|--address)
                HOST=${2} ; shift 2 ;;
            -p|--port)
                PORT=${2} ; shift 2 ;;
            -i|--install)
                INSTALL=true ; shift ;;
            -c|--copyssh)
                COPYSSH=true ; shift ;;
            -d|--debug)
                DEBUG=true ; shift ;;
            --)
                USER_AT_HOST=${2} ; shift ; break ;;
        esac
    done

    # Argument validation
    if [[ -z ${HOST} && -z ${USER_AT_HOST} ]]; then
        echo "ERROR: Host must be specified. Please see usage for details."
        exit 2
    fi
}

setup(){
    # Create variables along with default values
    local COPYSSH=false
    local DEBUG=false
    local INSTALL=false
    local HOST PASSWORD PORT USER extras

    # Parse command-line arguments
    parse_args ${ARGS}

    # Configure strings according to the parsed args
    if [ -z ${USER_AT_HOST} ]; then
        if [[ -z ${USER} ]]; then
            local USER_AT_HOST="${HOST}"
        else
            local USER_AT_HOST="${USER}@${HOST}"
        fi
    fi

    local SSH_OPTS="-oStrictHostKeyChecking=no -oUserknownHostsFile=/dev/null"
    local SSH_CMD="ssh ${SSH_OPTS}"
    local SCP_CMD="scp ${SSH_OPTS} -q -r"
    if [[ -n ${PORT} && ${PORT} -ne 22 ]]; then
        local SSH_CMD="${SSH_CMD} -p ${PORT}"
        local SCP_CMD="${SCP_CMD} -P ${PORT}"
    else
        PORT=22
    fi

    if ${DEBUG}; then
        set -x
    fi

    main
}

main() {
    if ${COPYSSH} || ${INSTALL}; then
        get_password_if_needed
        if ${COPYSSH}; then
            ${SSH_CP_ID} ${USER_AT_HOST} ${PORT} ${PASSWORD}
            local PASSWORD=
        fi
        if ${INSTALL}; then
            scp_confs true  # TODO This allows version downgrades
        fi
    fi
    run_interactive_ssh_cmd "${SYSTEM_CHECK_CMD} ${START_SSH_CHECK}" "-t -oNumberOfPasswordPrompts=0 -oBatchMode=yes"
    local RETCODE=${?}
    if [[ ${RETCODE} == 255 ]]; then  # Might need password- check
        attempt_password_path
    elif [[ ${RETCODE} != 0 ]]; then  # Something wasn't right- could be an odd exit code from initial ssh connection
        examine_exit_code
    fi
    set +x
}


get_password_if_needed(){
    if [[ -z ${PASSWORD} ]]; then
        local RETSTR=$(${SSH_CMD} -oNumberOfPasswordPrompts=0 -oBatchMode=yes ${USER_AT_HOST} -t "exit" 2>&1)
        if [[ ${RETSTR} =~ "Permission denied" ]]; then
            if ! which expect >/dev/null 2>&1 ; then
                echo "ERROR: ${PROGNAME} needs 'expect' installed to enable password authentication. Please either install expect, or manually setup passwordless ssh to '${USER_AT_HOST}'."
                exit 35
            fi
            echo -n "Password for ${USER_AT_HOST}: "
            read -s -t 30 password_tmp #TODO There is a way to disable echoing pw's. research
            PASSWORD=${password_tmp}
            local password_tmp=
        fi
    fi
}


attempt_password_path(){
    get_password_if_needed
    run_ssh_cmd "${SYSTEM_CHECK_CMD} exit 0" "-oNumberOfPasswordPrompts=0 -oBatchMode=yes"
    local RETCODE=${?}
    if [[ ${RETCODE} == 0 ]];then
        ${SSH_EXP_CMD_INTERACTIVE} ${USER_AT_HOST} ${PORT} ${PASSWORD} "${START_SSH_CHECK}"
    else
        examine_exit_code
    fi
}


examine_exit_code(){
    case ${RETCODE} in
        33)
            scp_confs_and_open_ssh ${INSTALL} ;;
        34)
            scp_confs_and_open_ssh true ;;
        *)
            echo "ERROR: Uncaught error code: ${RETCODE}" ; exit ${RETCODE} ;;
    esac
}


scp_confs_and_open_ssh(){
    scp_confs ${1}
    open_interactive_ssh
}


scp_confs(){
    local do_install=${1}
    # First, nuke the old confs
    run_ssh_cmd "if [ -e ~/${CONFDIR_DEST} ]; then rm -rf ~/${CONFDIR_DEST}; fi" >/dev/null 2>&1
    if [[ -n ${PASSWORD} ]]; then  # Case: Password required for authentication; use expect script
        ${SCP_EXP_CMD} ${CONFDIR} ${PORT} ${USER_AT_HOST}:~/${CONFDIR_DEST} ${PASSWORD}
    else  # Case: ssh key authentication set up; just call scp directly
       ${SCP_CMD} ${CONFDIR} ${USER_AT_HOST}:~/${CONFDIR_DEST}
    fi
    if ${do_install}; then
        run_ssh_cmd "~/.kommodum/install.sh" "" >/dev/null 2>&1
    fi
}


open_interactive_ssh(){
    if [[ -n ${PASSWORD} ]]; then   # Case: Still need password to connect
        ${SSH_EXP_CMD_INTERACTIVE} ${USER_AT_HOST} ${PORT} ${PASSWORD} "${START_SSH_CHECK}"
    else                          # Case: Nopasswd is set up
        run_interactive_ssh_cmd "${START_SSH_CHECK}"
    fi
}


run_ssh_cmd(){
    local CMD=${1}
    local MAS_SSH_OPTS=${2}
    if [[ -n ${PASSWORD} ]]; then  # Case: Password required for authentication; use expect script.
        ${SSH_EXP_CMD} ${USER_AT_HOST} ${PORT} ${PASSWORD} "${CMD}"
    else  # Case: ssh key authentication set up; just call SSH directly
        ${SSH_CMD} ${MAS_SSH_OPTS} ${USER_AT_HOST} "${CMD}"
    fi
}


run_interactive_ssh_cmd(){
    local CMD=${1}
    local MAS_SSH_OPTS=${2}
    if [[ -n ${PASSWORD} ]]; then
        ${SSH_EXP_CMD_INTERACTIVE} ${USER_AT_HOST} ${PORT} ${PASSWORD} "${CMD}"
    else
        ${SSH_CMD} ${MAS_SSH_OPTS} ${USER_AT_HOST} -t "${CMD}"
    fi
}


setup
